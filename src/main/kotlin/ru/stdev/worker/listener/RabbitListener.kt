package ru.stdev.worker.listener

import com.beust.klaxon.Klaxon
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.amqp.core.Message
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component
import ru.stdev.worker.common.Command
import ru.stdev.worker.common.SetImageOnTpiCommand
import javax.annotation.PostConstruct

@Component
class RabbitListener(
        private val objectMapper : ObjectMapper
) {
    private val logger = LoggerFactory.getLogger("RECEIVER")

    @ExperimentalStdlibApi
    @RabbitListener(queues = ["#{workerQueue.name}"], concurrency = "10")
    fun receive(rabbitMessage: Message) {
        logger.info("Receiver message: $rabbitMessage")

        try {

            val command = objectMapper.readValue(rabbitMessage.body.decodeToString(), Command::class.java)
            //val command = Klaxon().parse<Command>(rabbitMessage.body.decodeToString())
            command!!.doWork()

        } catch (e: Exception) {
            println(e)
        }
    }

    @PostConstruct
    fun test(){

        val json = "{\n" +
                "        \"command\": \"set_image_on_tpi\",\n" +
                "        \"invoker\": \"vpanshin\",\n" +
                "        \"image\": \"img.jpeg\",\n" +
                "        \"device_id\": 67,\n" +
                "        \"ip\": \"192.168.1.1\",\n" +
                "        \"port\": 67\n" +
                "    }"

        val command = objectMapper.readValue(json, Command::class.java)
        command.doWork()
        println()
    }
}