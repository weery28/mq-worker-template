package ru.stdev.worker

import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component
import ru.stdev.worker.common.Command

@Component
class MessageLogger {
    private val logger = LoggerFactory.getLogger("LOGGER")
}