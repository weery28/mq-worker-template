package ru.stdev.worker.config

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.core.Declarables
import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.TopicExchange
import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@EnableRabbit
@Configuration
class RabbitConfiguration {

    @Value("\${worker.name}")
    lateinit var workerName: String

    @Bean
    fun taskExchange() = TopicExchange("tasks")

    @Bean
    fun workerQueue() = Queue(workerName, false)

    @Bean
    fun topicBindings(
            incomingExchange: TopicExchange,
            workerQueue: Queue
    ): Declarables {
        return Declarables(
                workerQueue,
                incomingExchange,
                BindingBuilder
                        .bind(workerQueue)
                        .to(incomingExchange).with("*")
        )
    }

    @Bean
    fun jackson2MessageConverter(): MessageConverter {
        return Jackson2JsonMessageConverter(
                jacksonObjectMapper()
                        .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
                        .setSerializationInclusion(JsonInclude.Include.NON_NULL)
        )
    }
}