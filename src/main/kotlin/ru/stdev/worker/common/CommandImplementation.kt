package ru.stdev.worker.common

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class CommandImplementation(
        val name : String
)