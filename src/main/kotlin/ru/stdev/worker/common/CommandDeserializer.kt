package ru.stdev.worker.common

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.jackson.JsonComponent
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.context.annotation.Lazy
import org.springframework.core.type.filter.AnnotationTypeFilter
import javax.annotation.PostConstruct


@JsonComponent
class CommandDeserializer(
        @Lazy private val objectMapper: ObjectMapper,
        @Value("\${command.deserializer.basepackage:}") private val basePackageName : String
): JsonDeserializer<Command>() {

    private lateinit var bindings : Map<String, Class<Command>>
    override fun deserialize(parser: JsonParser, context: DeserializationContext): Command {

        val node = parser.readValueAsTree<JsonNode>()
        val commandName = node.get("command").asText()

        val parentClass = bindings[commandName]?:
        throw IllegalArgumentException("Deserializer does not contains implementation binding for $commandName ")

        return objectMapper.convertValue<Command>(node, parentClass)
    }

    @PostConstruct
    private fun registerBindings() {


        val provider = ClassPathScanningCandidateComponentProvider(false)
        provider.addIncludeFilter(AnnotationTypeFilter(CommandImplementation::class.java))

        bindings = provider.findCandidateComponents(basePackageName)
                .asSequence()
                .map { Class.forName(it.beanClassName) }
                .map {
                    if (it.superclass == Command::class.java) {
                        it as Class<Command>
                        it.getAnnotation(CommandImplementation::class.java).name to it
                    } else {
                        throw IllegalArgumentException("Only implementations of Command interface can be annotated with CommandImplementation")
                    }
                }
                .toMap()
    }
}