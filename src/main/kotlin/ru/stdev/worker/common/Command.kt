package ru.stdev.worker.common

import com.beust.klaxon.Json
import com.beust.klaxon.TypeAdapter
import com.beust.klaxon.TypeFor
import kotlin.reflect.KClass

interface ProcessableCommand {
    fun doWork(): Any?
}

//@TypeFor(field = "command", adapter = CommandTypeAdapter::class)
abstract class Command(
        val invoker: String = "Unknown"
) : ProcessableCommand {
    abstract val command: String
}

@CommandImplementation("set_image_on_tpi")
data class SetImageOnTpiCommand(
        override val command: String,
        val image: String,
        @Json("device_id")
        val deviceId: Long,
        val ip: String,
        val port: Int
) : Command() {

    override fun doWork(): String {
        println("${this.command} start job......")
        println("Invoker: ${this.invoker}")
        println("${this.command} end job......")
        return ""
    }
}

//class CommandTypeAdapter() : TypeAdapter<Command> {
//        override fun classFor(type: Any): KClass<out Command> = when (type as String) {
//        "set_image_on_tpi" -> SetImageOnTpiCommand::class
//        else -> throw IllegalArgumentException("Unknown command $type")
//    }
//}

/*
JSON command example:
    {
        "command": "set_image_on_tpi",
        "invoker": "vpanshin",
        "image": "img.jpeg",
        "device_id": 67,
        "ip": "192.168.1.1",
        "port": 67
    }
*/